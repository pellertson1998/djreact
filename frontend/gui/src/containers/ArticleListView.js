import React from 'react';
import axios from 'axios';
import Articles from '../components/Article';

class ArticleList extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			articles: [],
		};
	}

	componentDidMount() {
		var articleData = axios.get('http://localhost:8000/api/');
		articleData.then(res => {
			this.setState({
				articles: res.data
			});
		});
	}

	render() {
		return (
			<Articles data={this.state.articles} />
		);
	}
}

export default ArticleList;