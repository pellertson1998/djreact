import React from 'react';
import axios from 'axios';
import { Card } from 'antd';

class ArticleDetail extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			article: {},
		};
	}

	componentDidMount() {
		var articleID = this.props.match.params.articleID,
		    articleData = axios.get(`http://localhost:8000/api/${articleID}`);

		articleData.then(res => {
			this.setState({
				article: res.data
			});
		});
	}

	render() {
		return (
			<Card title={this.state.article.title}>
				<p>{this.state.article.content}</p>
			</Card>
		);
	}
}

export default ArticleDetail;