from django.db import models

default_url = "https://vignette.wikia.nocookie.net/jjba/images/3/3b/Jotaro_first_line.png/revision/latest?cb=20190810234114"

class Article(models.Model):
	title   = models.CharField(max_length=120)
	content = models.TextField()
	logo    = models.URLField(default=default_url)

	def __str__(self):
		return self.title

