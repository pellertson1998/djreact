# Generated by Django 2.2.5 on 2019-09-16 21:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0002_auto_20190916_1840'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='logo',
            field=models.URLField(default='https://vignette.wikia.nocookie.net/jjba/images/3/3b/Jotaro_first_line.png/revision/latest?cb=20190810234114'),
        ),
    ]
